/*
** my_str_pure.c for  in /home/poulet_a/projets/minishell2/std_epitech
** 
** Made by poulet_a
** Login   <poulet_a@epitech.net>
** 
** Started on  Tue Feb 18 14:23:09 2014 poulet_a
** Last update Fri Mar  7 09:07:31 2014 poulet_a
*/

#include "my.h"

char    my_str_epure_quote(char c, char cprec)
{
  if ((c == '\'' || c == '\"') && cprec != '\\')
    return (c);
  return (0);
}

void	my_str_epure_space(char *str,
			   char *out,
			   int i_n[2],
			   char quo_spa[2])
{
  if ((i_n[0] > 0 && my_str_epure_quote(str[i_n[0]], str[i_n[0] - 1]))
      || (i_n[0] == 0 && my_str_epure_quote(str[i_n[0]], 0)))
    quo_spa[0] = str[i_n[0]];
  if (str[i_n[0]] != ' ' || quo_spa[1] == 0)
    {
      if (str[i_n[0]] == ' ')
	quo_spa[1] = 1;
      else
	quo_spa[1] = 0;
      if (!(!str[i_n[0] + 1] && str[i_n[0]] == ' '))
	out[i_n[1]] = str[i_n[0]];
      else
	out[i_n[1]] = 0;
      i_n[1]++;
    }
}

void	my_str_epure_post(char *out, int i_n[2], char quo_spa[2])
{
  if (quo_spa[1] == 1 && i_n[1] >= 1)
    out[i_n[1] - 1] = 0;
  else if (i_n[1] >= 0)
    out[i_n[1]] = 0;
  else
    out[0] = 0;
}

/*
** RET_NULL_NULL(str); Pointless bc of my_strlen'll return LONE
** remove first and last " ",
** then remove " " only if they follow an other and if they are not in a '' ""
** quo_spa[0] is ignore and quo_spa[1] is spaces
*/
char	*my_str_epure(char *str)
{
  char	*out;
  int	i_n[2];
  char  quo_spa[2];

  RET_NULL_NULL(str);
  RET_NULL_NULL((out = malloc(my_strlen(str) + 2)));
  THREE_ZERO(i_n[0], i_n[1], quo_spa[0]);
  quo_spa[1] = 2;
  while (str[i_n[0]])
    {
      if (quo_spa[0] != 0)
	{
	  quo_spa[0] = (str[i_n[0]] == quo_spa[0]) ? (0) : (quo_spa[0]);
	  out[i_n[1]++] = str[i_n[0]++];
	}
      else
	{
	  my_str_epure_space(str, out, i_n, quo_spa);
	  i_n[0]++;
	}
    }
  my_str_epure_post(out, i_n, quo_spa);
  return (out);
}
