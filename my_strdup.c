/*
** my_strdup.c for  in /home/poulet_a/projets/minishell1/src
** 
** Made by poulet_a
** Login   <poulet_a@epitech.eu>
** 
** Started on  Mon Dec 16 15:30:14 2013 poulet_a
** Last update Thu Mar  6 14:11:25 2014 poulet_a
*/

#include <stdlib.h>
#include "my.h"

char	*my_strdup(char *src)
{
  int	i;
  int	imax;
  char	*dup;

  i = 0;
  RET_NULL_NULL(src);
  RET_LONE_NULL((imax = my_strlen(src)));
  RET_NULL_NULL((dup = malloc(imax + 1)));
  while (i < imax)
    {
      dup[i] = src[i];
      i++;
    }
  dup[i] = 0;
  return (dup);
}
